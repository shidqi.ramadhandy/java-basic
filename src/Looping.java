import java.util.Arrays;
import java.util.List;

public class Looping {
    public static void main(String[] args) {
        sampel1(10);
        sampel2(10);
        sampel3(10);
        sampel4();

    }

    public static void sampel1(int n){
        for (int i=0; i<=n; i++){
            System.out.print(i + "\t");
        }
        System.out.println("\n ===========================");
    }

    public static void sampel2(int n){
        for (int i=0; i<=n; i++){
            if (i %2 == 0) {
                System.out.print("EA \t");
            } else {
                System.out.print(i + "\t");
            }

        }
        System.out.println("\n ===========================");
    }

    public static void sampel3(int n){
        for (int i=0; i<=n; i++){
            for (int j=0; j<=n; j++){
                System.out.print("["+i+","+j+"] \t");
            }
            System.out.println("\n");
        }
        System.out.println("\n ===========================");
    }

    public static void sampel4(){
        List<String> listString = Arrays.asList("Milk", "Choho Milk", "Strawberry Milk");
        System.out.println("List of Milk: ");
        for(String item: listString){
            System.out.println(item);
        }
        System.out.println("\n ===========================");
        System.out.println("List of Number: ");
        List<Integer> listNumber = Arrays.asList(1,2,3,4,5);
        for (Integer item: listNumber){
            System.out.println(item)
            ;
        }
    }


}
