public class IfNested {
    public static void main(String[] args) {

        nestedIf(12);

        paralelIf(15);
    }

    //Nested
    public static void nestedIf(int angka){
        if (angka > 0){
            //Modulo == Sisa hasil bagi
            if (angka % 2 == 0){
                int result = angka*2;
                System.out.println("hasil Genap: "+ result);
            } else {
                int result = angka*3;
                System.out.println("hasil Ganjil: "+ result);
            }
        }else {
            System.out.println("Angka kurang dari 0");
        }
    }

    public static void paralelIf(int angka){
        if(angka > 0){
            System.out.println("Angka lebih dari 0");
        }

        if(angka % 2 == 0){
            System.out.println("Angka Genap");
        }else {
            System.out.println("Angka Ganjil");
        }

        if(angka / 2 >= 5){
            System.out.println("Angka 10 keatas");
        }else {
            System.out.println("Angka kurang dari 10");
        }
    }

}
